# WupExotTools

This tool is designed to produce common n-tuples for a LQ and a VLQ analysis using the AnalysisTop (AT) framework. 

# Content of the package

# Production of n-tuples

# Useful links:

Top-reco repo: https://gitlab.cern.ch/atlasphys-top/reco
* Tools for checking AT output (e.g. TrackingDataAnalysis to check tool configuration)

Analysis Top repo: https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/TopPhys/xAOD
* Source code of the AT framework

Recommendations for top systematics
* https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopMCSystematicsR21
* https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PmgTopProcesses

Overlap Removal
* https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils

EXOT4 derivation
* https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/DerivationFramework/DerivationFrameworkExotics/share/EXOT4.py


Useful links for analysis
* tt-Wt Interference Unc.: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/WWbbUncerts
