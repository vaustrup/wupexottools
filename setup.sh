export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS

cd build
asetup --restore 
source */setup.sh
cd ../run

lsetup panda
export RUCIO_ACCOUNT=$CERN_USER
lsetup "rucio -w"
lsetup pyami
