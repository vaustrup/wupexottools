#include "WupExotTools/WupExotEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopEvent/EventTools.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopConfiguration/TopConfig.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include "MuonSelectorTools/MuonSelectionTool.h"
#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "TRandom3.h"

#define CHECK_CPCorr(Arg) \
  if (Arg.code() == CP::CorrectionCode::Error){    \
    Error(#Arg,"Correction Code 'Error' (returned in line %i) ",__LINE__); \
  }

#define CHECK_CPSys(Arg) \
  if (Arg.code() == CP::SystematicCode::Unsupported){    \
    Warning(#Arg,"Unsupported systematic (in line %i) ",__LINE__); \
  } 



namespace top{
  ///-- Constructor --///
  WupExotEventSaver::WupExotEventSaver() :
    m_config(nullptr),
    m_savetruth(false),
    m_chargedLeptonDecayLQ(0.),
    m_chargedLeptonDecayAntiLQ(0.)
  {
  }

  

  


  ///-- initialize - done once at the start of a job before the loop over events --///
  void WupExotEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

    ///-- initialize config and sample infos --///
    m_config    = config;
    m_savetruth = (m_config->isMC()) ? true : false;

  
     ///-- declare METsig --//
    // set to SUSYTool defaults; additional options can be found under Reconstruction/MET/METUtilities/Root/METSignificance.cxx

    std::string jetCalibrationName = m_config->sgKeyJetsType();
    jetCalibrationName.erase(jetCalibrationName.length() - 4);
    
    metSignif.setTypeAndName("met::METSignificance/metSignificance");
    top::check(metSignif.setProperty("SoftTermParam", met::Random),"Failed to set SoftTermPara");
    top::check(metSignif.setProperty("TreatPUJets", false),"Failed to set PUJets");
    top::check(metSignif.setProperty("DoPhiReso",true),"Failed to set DoPhireso");
    top::check(metSignif.setProperty("IsAFII",  m_config->isAFII()),"Failed to set AF" ); 
    top::check(metSignif.setProperty("JetCollection",jetCalibrationName),"Failed to set JetCollectionName"); //Has to be set appropiate e.g. AntiKt4EMPFlow
    top::check(metSignif.retrieve(),"Failed to retrieve metSignif" );

     ///-- declare HTsig --//

    jerTool.setTypeAndName("JERTool/MyJERTool");
    top::check(jerTool.retrieve(),"Failed to retrieve jerTool");
    
    /// -- setup muon tool for high pt --//
    // muon quality 0:Tight; 1:Medium; 2:Loose; 3:VeryLoose;4 HighPt
    m_muonSelectionToolHighPt.setTypeAndName("CP::MuonSelectionTool/Selection_HighPt");
    top::check(m_muonSelectionToolHighPt.setProperty("MuQuality", 4), "Failed to set MuQuality for highPt");
    top::check(m_muonSelectionToolHighPt.setProperty("MaxEta", m_config->muonEtacut()), "Failed to set MaxEta for highPt" );
    top::check(m_muonSelectionToolHighPt.retrieve(), "Failed to retrieve muonSelectionToolHighPt");

    /// -- setup muon tool for normal muon selection --//
    m_muonSelectionToolNormal.setTypeAndName("CP::MuonSelectionTool/Selection_Normal");
    top::check(m_muonSelectionToolNormal.setProperty("MuQuality",1), "Failed to set MuQuality for normal muon selection");
    top::check(m_muonSelectionToolNormal.setProperty("MaxEta",m_config->muonEtacut()), "Failed to set MaxEta for normal muon selection" );
    top::check(m_muonSelectionToolNormal.retrieve(), "Failed to retrieve muonSelectionToolNormal");


    /// -- setup muon efficiency scale factor tool for high pt --//
    m_muonEfficiencyScaleFactorToolHighPt.setTypeAndName("CP::MuonEfficiencyScaleFactors/ScaleFactor_HighPt");
    top::check(m_muonEfficiencyScaleFactorToolHighPt.setProperty("WorkingPoint","HighPt"), "Failed to set working point for highPt" ); 
    top::check(m_muonEfficiencyScaleFactorToolHighPt.retrieve(), "Failed to retrieve muonEfficiencyScaleFactorToolHighPt" );

    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      //event info
      systematicTree->makeOutputVariable(m_lumiBlock, "lbn");
      systematicTree->makeOutputVariable(m_vtxz,      "Vtxz");
      systematicTree->makeOutputVariable(m_npvtx,     "npVtx");

      systematicTree->makeOutputVariable(m_met_sig, "METsig");
      systematicTree->makeOutputVariable(m_ht_sig, "HTsig");

      systematicTree->makeOutputVariable(m_bad_muon_highpt, "BadMuonHighPt");
      systematicTree->makeOutputVariable(m_bad_muon_normal, "BadMuonNormal");
      systematicTree->makeOutputVariable(m_passHighPt, "passHighPt");

      if(m_savetruth){

        systematicTree->makeOutputVariable(m_SF_ID_HighPt, "SF_ID_HighPt");
                                          
        if(isNominalTree(systematicTree->name())){
          systematicTree->makeOutputVariable(m_nTruthJets, "nTruthJets");
          
          systematicTree->makeOutputVariable(m_SF_ID_STAT_UP_HighPt, "SF_ID_HighPt_STAT_UP");
          systematicTree->makeOutputVariable(m_SF_ID_STAT_DOWN_HighPt, "SF_ID_HighPt_STAT_DOWN");
          systematicTree->makeOutputVariable(m_SF_ID_SYST_UP_HighPt, "SF_ID_HighPt_SYST_UP");
          systematicTree->makeOutputVariable(m_SF_ID_SYST_DOWN_HighPt, "SF_ID_HighPt_SYST_DOWN");
        }

            ///-- LQ variables --///
        systematicTree->makeOutputVariable(m_chargedLeptonDecayLQ, "chargedLeptonDecayLQ");
        systematicTree->makeOutputVariable(m_chargedLeptonDecayAntiLQ, "chargedLeptonDecayAntiLQ");
        systematicTree->makeOutputVariable(m_lq_truth_pt, "lq_truth_pt");
        systematicTree->makeOutputVariable(m_antilq_truth_pt, "antilq_truth_pt");

        ///-- VLQ variables --///
        systematicTree->makeOutputVariable(m_vlq_evtype, "vlq_evtype");
        systematicTree->makeOutputVariable(m_numZnunu, "numZnunu");

        ///-- truth met --//
        systematicTree->makeOutputVariable(m_truthmet, "truthmet");
        systematicTree->makeOutputVariable(m_GenFiltHT, "GenFiltHT");
        systematicTree->makeOutputVariable(m_GenFiltHTnew,"GenFiltHTnew");
        systematicTree->makeOutputVariable(m_GenFiltHTinclNu, "GenFiltHTinclNu");

        ///-- ttbar category --///
        systematicTree->makeOutputVariable(m_ttbar_cat, "ttbar_cat");
        systematicTree->makeOutputVariable(m_truth_top_pt, "truth_top_pt");
        systematicTree->makeOutputVariable(m_truth_antitop_pt, "truth_antitop_pt");
      }
    }
  }
  
  ///-- saveEvent - run for every systematic and every event --///
  void WupExotEventSaver::saveEvent(const top::Event& event) 
  {
    
    m_chargedLeptonDecayLQ = -1.;
    m_chargedLeptonDecayAntiLQ = -1.;
    m_lq_truth_pt = -1.;
    m_antilq_truth_pt = -1.;

    m_nTruthJets = -1;

    m_vlq_evtype = -1;
    m_numZnunu = -1;

    m_truthmet = -1.;

    m_ttbar_cat = -1;
    m_truth_top_pt = -1;
    m_truth_antitop_pt = -1;

    m_met_sig=-1;
    m_ht_sig= -1;

    m_bad_muon_highpt = 0;
    m_bad_muon_normal = 0;

    //event info
    if (!(top::isSimulation(event))) m_lumiBlock   = event.m_info->lumiBlock();
    //// primary vertices
    m_npvtx = 0;
    m_vtxz  = 0;
    const xAOD::VertexContainer *m_primvtx = event.m_primaryVertices;
    for (const auto* const vtxPtr : *m_primvtx) {
      const xAOD::VxType::VertexType vtype = vtxPtr->vertexType();
      const int vmult = vtxPtr->trackParticleLinks().size();
      // count vertices of type 1 (primary) and 3 (pileup) with >4 tracks
      if ((vtype==1 || vtype==3) && vmult>4) {
	++m_npvtx;
	// assuming there is only one primary vertex
	if (vtype==1) m_vtxz = vtxPtr->z();
      }
    }
    m_SF_ID_HighPt=1;
    m_SF_ID_STAT_UP_HighPt=1;
    m_SF_ID_STAT_DOWN_HighPt=1;
    m_SF_ID_SYST_UP_HighPt=1;
    m_SF_ID_SYST_DOWN_HighPt=1;
    
    m_passHighPt = false;

    // -> muon selection tool
    float scale_factor = 1.0;
    for (const auto* const muPtr : event.m_muons) {       
      if (m_muonSelectionToolHighPt->isBadMuon(*muPtr)) {    
        m_bad_muon_highpt = true;        
      }
      if (m_muonSelectionToolNormal->isBadMuon(*muPtr)) {
        m_bad_muon_normal = true;
      }
      if(m_muonSelectionToolHighPt->accept(*muPtr)){
      m_passHighPt = true;
      for (auto& syst : CP::make_systematics_vector(m_muonEfficiencyScaleFactorToolHighPt->recommendedSystematics())) {
        scale_factor = 1;
        CHECK_CPSys(m_muonEfficiencyScaleFactorToolHighPt->applySystematicVariation(syst));
        CHECK_CPCorr(m_muonEfficiencyScaleFactorToolHighPt->getEfficiencyScaleFactor(*muPtr,scale_factor));
        if(syst.name()=="") m_SF_ID_HighPt = scale_factor;
        else if(syst.name()=="MUON_EFF_RECO_STAT__1up") m_SF_ID_STAT_UP_HighPt = scale_factor;
        else if(syst.name()=="MUON_EFF_RECO_STAT__1down") m_SF_ID_STAT_DOWN_HighPt = scale_factor;
        else if(syst.name()=="MUON_EFF_RECO_SYS__1up") m_SF_ID_SYST_UP_HighPt = scale_factor;
        else if(syst.name()=="MUON_EFF_RECO_SYS__1down") m_SF_ID_SYST_DOWN_HighPt = scale_factor;
      }
      }
    }

     ///-- calculate METsig --//
    
    xAOD::MissingETContainer* mets(nullptr);
    if (!event.m_isLoose) {
     top::check(evtStore()->retrieve(mets, m_config->sgKeyMissingEt(event.m_hashValue)), "Failed to retrieve MET");
    }
    else {
     top::check(evtStore()->retrieve(mets, m_config->sgKeyMissingEtLoose(event.m_hashValue)), "Failed to retrieve MET");
    }
    
    top::check(metSignif->varianceMET(mets, event.m_info->averageInteractionsPerCrossing(), "RefJet", "PVSoftTrk", "FinalTrk"), "Failed to calculate METsign"); //
    m_met_sig = metSignif->GetSignificance();



    ///--calculate HTsig --///
    
    const bool isData = false;

    TRandom3 myrandom(event.m_info->eventNumber());

    double METxLEP=0.;
    double METyLEP=0.;
    double sumET = 0.;
    TLorentzVector HTvec = TLorentzVector(0.,0.,0.,0.);

     //base electron.
  
    for (const auto* const elPtr: event.m_electrons){     
      float ref_pt =  (elPtr)->pt();
      float ref_phi = (elPtr)->phi();        
      METxLEP -= ref_pt*cos(ref_phi);
      METyLEP -= ref_pt*sin(ref_phi);
      sumET+=ref_pt;
      HTvec-=(elPtr)->p4();
    }

    //base muons.
    for (const auto* const muPtr: event.m_muons){ 
     
      float ref_pt = (muPtr)->pt();
      float ref_phi =(muPtr)->phi();

      METxLEP -= ref_pt*cos(ref_phi);
      METyLEP -= ref_pt*sin(ref_phi);
      sumET+=ref_pt;
      HTvec-=(muPtr)->p4();
    }

    double ETmissmean=0;
    double ETmissRMS=0;
    for (int ientry =0; ientry<1000; ientry++) {
      double holdx=0;
      double holdy =0;
    
      double sigmas = 1.; 
     
      for (auto jet_itr = event.m_jets.begin(); jet_itr != event.m_jets.end(); ++jet_itr) {
	    
	    float ref_pt = (*jet_itr)->pt();
	    float ref_phi = (*jet_itr)->phi();
	    float jetpx = ref_pt*cos(ref_phi);
	    float jetpy = ref_pt*sin(ref_phi);
	    if (ientry==0){ 
              sumET+=ref_pt;
	      HTvec-=(*jet_itr)->p4();
	    }

	    const xAOD::Jet* jet = *jet_itr;
	    if (isData){
	        sigmas= jerTool->getRelResolutionData(jet);
	    }
	    else {
	        sigmas = jerTool->getRelResolutionMC(jet);
	    }

	    holdx -= myrandom.Gaus(jetpx, sigmas * jetpx) * 0.001;
	    holdy -= myrandom.Gaus(jetpy, sigmas * jetpy) * 0.001;
      }
      holdx += METxLEP * 0.001;
      holdy += METyLEP * 0.001;
      
      double holdMET = sqrt(holdx*holdx + holdy*holdy);
      ETmissmean += holdMET;
      ETmissRMS += holdMET*holdMET;
    }
    ETmissmean = ETmissmean * 0.001;//   /n_tries
    ETmissRMS = ETmissRMS * 0.001 - ETmissmean*ETmissmean;
    m_ht_sig = (ETmissmean - scale)/sqrt(ETmissRMS);


    //
    if(m_savetruth){
      int mcChannelNumber = event.m_info->mcChannelNumber();

      const xAOD::JetContainer* truthJets(nullptr);
      top::check( evtStore()->retrieve( truthJets, "AntiKt4TruthWZJets"), "Failed to retrieve truth jets container.");
      m_nTruthJets = 0;
      for( const xAOD::Jet* truthJet: *truthJets ){
        if( truthJet->pt()<20000. || std::abs(truthJet->eta()) >= 2.8){
          continue;
        }
        static const SG::AuxElement::ConstAccessor<int> acc("HadronConeExclTruthLabelID");
        if(acc.isAvailable(*truthJet) && ( acc(*truthJet) == 15)){
          continue;
        }
        ++m_nTruthJets;
      }


      //--- Classify LQs ---//
      if( (mcChannelNumber >= m_lq_dsids.at(0) && mcChannelNumber <= m_lq_dsids.at(1))|| (mcChannelNumber >= m_lq_dsids_ext.at(0) && mcChannelNumber <= m_lq_dsids_ext.at(1)) || (mcChannelNumber >= m_vectorlq_dsids.at(0) && mcChannelNumber <= m_vectorlq_dsids.at(1)) || (mcChannelNumber >= m_vectorlq_dsids_ext.at(0) && mcChannelNumber <= m_vectorlq_dsids_ext.at(1))){
	    
        bool lq_found = 0;
	      bool antilq_found = 0;

        int pdgIdA = 42;
        int pdgIdB = 43;
        if((mcChannelNumber >= m_vectorlq_dsids.at(0) && mcChannelNumber <= m_vectorlq_dsids.at(1)) || (mcChannelNumber >= m_vectorlq_dsids_ext.at(0) && mcChannelNumber <= m_vectorlq_dsids_ext.at(1))){
          pdgIdA = 9000007;
          pdgIdB = 9000007;
        }

        const xAOD::TruthParticleContainer* truthParticles(nullptr);
        top::check( evtStore()->retrieve( truthParticles, "TruthParticles" ) , "Failed to retrieve truth particle container");

        for ( const auto* truthP : *truthParticles ) {
	        // status seems to be 62 for decaying LQs; prevents errors when LQ chain cannot be fully reconstructed (reason?)
	        if(!lq_found && (truthP->pdgId() == pdgIdA || truthP->pdgId() == pdgIdB) && truthP->status()==62 ){
	            if(truthP->pdgId() != 9000007) m_chargedLeptonDecayLQ = decayToChargedLepton(truthP);
              else m_chargedLeptonDecayLQ = decayToChargedLeptonVLQ(truthP);
	            m_lq_truth_pt = truthP->pt();
	            lq_found = 1;
	        }
	        else if(!antilq_found && (truthP->pdgId() == -pdgIdA || truthP->pdgId() == -pdgIdB) && truthP->status()==62){
	            if(truthP->pdgId() != -9000007) m_chargedLeptonDecayAntiLQ = decayToChargedLepton(truthP);
              else m_chargedLeptonDecayAntiLQ = decayToChargedLeptonVLQ(truthP);
	            m_antilq_truth_pt = truthP->pt();
	            antilq_found = 1;
	        }
	        else if(lq_found==1 && antilq_found==1){
	            break; //check and test this!!!
	        }
        }
        if(!lq_found && !antilq_found){
            m_chargedLeptonDecayAntiLQ = -1;
            m_chargedLeptonDecayLQ = -1;
        }
        if((!lq_found && antilq_found) || (lq_found && !antilq_found)) std::cout << "Only one LQ found." << std::endl;
      } // end LQ classification
    
	

      ///-- Classify VLTs ---///
       if(std::find(m_vlq_dsids.begin(), m_vlq_dsids.end(), mcChannelNumber) != m_vlq_dsids.end()){
         //if( mcChannelNumber >= m_vlq_dsids.at(0) && mcChannelNumber <= m_vlq_dsids.at(1) ){
	
	unsigned int haveWq = 0;
	unsigned int haveZq = 0;
	unsigned int haveHq = 0;
	int particleID      = 0; // keep track of VLQ particle ID (7 or 8)
	m_numZnunu      = 0; // set to 0 for VLQ sample
	
	//Container of truth particles
	const xAOD::TruthParticleContainer *truth_particles(nullptr);
	top::check( evtStore()->retrieve(truth_particles,m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );
	
	for (const auto* const particle : *truth_particles) {       
	  
	  // Multiple heavy quarks, [old: restrict to those that have 2 daughters]
	  // Status always 62? Appears so for T and B
	  // Checked unique pair, and 2nd child is the boson
	  if ( (particle->absPdgId()== 7 || particle->absPdgId()== 8)
	       && particle->status() == 62
	       && particle->hasDecayVtx()){

	    if (particle->child(0)->isQuark()){
	      
	      if (particle->child(1)->isW())
		    haveWq++; // Wb!
	      else if (particle->child(1)->isZ())
		    haveZq++; // Zt!
	      else if (particle->child(1)->isHiggs())
		    haveHq++; // Ht!

		  particleID = particle->absPdgId();
	
	      
	    } // endif children are quark+boson
	  } // endif good vlq particle
	
	  if (haveWq+haveZq+haveHq == 2){
	    // if the sum is 2, then we've discovered the T and Tbar.
	    // No need to keep looping after we're done!
	    break;
	  }
	
	} // end for loop over truth particles
	
	if (haveWq == 2 && haveZq == 0 && haveHq == 0){
	  m_vlq_evtype = 0;
	}
	else if (haveWq == 0 && haveZq == 2 && haveHq == 0){
	  m_vlq_evtype = 1;
	}
	else if (haveWq == 0 && haveZq == 0 && haveHq == 2){
	  m_vlq_evtype = 2;
	}
	else if (haveWq == 1 && haveZq == 1 && haveHq == 0){
	  m_vlq_evtype = 3;
	}
	else if (haveWq == 1 && haveZq == 0 && haveHq == 1){
	  m_vlq_evtype = 4;
	}
	else if (haveWq == 0 && haveZq == 1 && haveHq == 1){
	  m_vlq_evtype = 5;
	}
	else{
	  m_vlq_evtype = -1;
	}

	if (particleID==7 && m_vlq_evtype != -1) m_vlq_evtype+=6;

	// search for Z bosons decaying into neutrinos
	int Znunu = 0;
	for (const auto* const particle : *truth_particles) {
	  if(!particle->isZ())
	    continue;
	  if (particle->nChildren() > 1){
	    if (particle->child(0)->isNeutrino() && particle->child(1)->isNeutrino()) {
	      Znunu++;
	    }
	  }
	  
	}//end for-loop
	m_numZnunu = Znunu;
	
	 } // end VLT classification
	
   /// -- Truth HT -- ///
   
   if(event.m_info->isAvailable<float>("GenFiltHTinclnu")) m_GenFiltHTinclNu = event.m_info->auxdata<float>("GenFiltHTinclNu");
   if(event.m_info->isAvailable<float>("GenFiltHT")) m_GenFiltHT = event.m_info->auxdata<float>("GenFiltHT");
   const xAOD::TruthParticleContainer *truthParticles(nullptr);
        top::check( evtStore()->retrieve(truthParticles,m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );
        const xAOD::JetContainer *truthjets(nullptr);
        top::check( evtStore()->retrieve(truthjets,"AntiKt4TruthWZJets") , "Failed to retrieve TruthJets" );


        m_GenFiltHTnew = 0;
        for (const auto& tj : *truthjets) {
          if ( tj->pt()>35e3 && fabs(tj->eta())<2.5 ) {
          ATH_MSG_VERBOSE("Adding truth jet with pt " << tj->pt()
                        << ", eta " << tj->eta()
                        << ", phi " << tj->phi()
                        << ", nconst = " << tj->numConstituents());
          m_GenFiltHTnew += tj->pt();
          }
        }

      for (const auto& tp : *truthParticles){
        int pdgid = tp->pdgId();
        if (tp->barcode() >= 200000) continue; // Particle is from G4
        if (pdgid==21 && tp->e()==0) continue; // Work around for an old generator bug
        if ( tp->status() %1000 !=1 ) continue; // Stable!
        bool addpart = false;
        if ((abs(pdgid)==11 || abs(pdgid)==13) && tp->pt()>25e3 && fabs(tp->eta())<2.5) {
           const xAOD::TruthParticle* par = tp->parent(0);
           while ( par ) {
             // when there is a hadron somewhere we are not interested
             if (par->isHadron() )
               break;

             if ( (par->isW() || par->isZ() || abs(par->pdgId())<9) || abs(par->pdgId())==15) {
               addpart = true;
               break;
             }
             par = par->parent(0);
           }
        }
        if(addpart) {
          ATH_MSG_VERBOSE("Adding prompt lepton with pt " << tp->pt()
                          << ", eta " << tp->eta()
                          << ", phi " << tp->phi()
                          << ", status " << tp->status()
                          << ", pdgId " << pdgid);
          m_GenFiltHTnew += tp->pt();
        }
      }


	 /// -- Truth MET --///

	 if(std::find(m_truthmet_dsids.begin(), m_truthmet_dsids.end(), mcChannelNumber) != m_truthmet_dsids.end()){
	const xAOD::TruthParticleContainer *truthParticles(nullptr);
	top::check( evtStore()->retrieve(truthParticles,m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );
	// code from Frederik Ruehr
	 double sumx = 0;
	 double sumy = 0;
	
	 for(const auto* const particle : *truthParticles) { 
	   //for(const auto& particle : *truthParticles) {
    
	   // find all status=1 neutrinos
	   bool addpart = false;
	   if (particle->status() != 1) 
	     continue;
	   if (!particle->isNeutrino()) 
	     continue;
	 
	   // loop through the parents and see if its from a W, Z or |pdg|<9
	   const xAOD::TruthParticle* par = particle->parent(0);
	   while ( par ) {
	     // when there is a hadron somewhere we are not interested
	     if (par->isHadron() ) 
	       break;

	     if ( (par->isW() || par->isZ() || abs(par->pdgId())<9) ) {
	       addpart = true; 
	       break;
	     }

	     par = par->parent(0);
	   }

	   if (addpart) {
	     sumx += particle->p4().Px();
	     sumy += particle->p4().Py();
	   }
	 }

	 m_truthmet = sqrt(sumx*sumx + sumy*sumy);

	 }
	// truth top pt for alternative ttbar samples
	if(std::find(m_ttbar_dsids_alt.begin(), m_ttbar_dsids_alt.end(), mcChannelNumber) != m_ttbar_dsids_alt.end()){
	  const xAOD::TruthParticleContainer* truthParticles(nullptr);
      top::check( evtStore()->retrieve(truthParticles,m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );
	  for (const auto& particle: *truthParticles) {
	    if (particle->pdgId() == 6){ //take always the last top
	      m_truth_top_pt = particle->pt();
	     }
	      if (particle->pdgId() == -6){ //take always the last top
	      m_truth_antitop_pt = particle->pt();
	     }
	  }
	}
	
	/// -- ttbar classification only for nominal ttbar--///

	if(std::find(m_ttbar_dsids.begin(), m_ttbar_dsids.end(), mcChannelNumber) != m_ttbar_dsids.end()){
	   const xAOD::TruthParticleContainer *truthParticles(nullptr);
	   top::check( evtStore()->retrieve(truthParticles,m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );
	 

	   DecayMode topDecay = ERROR;
	   DecayMode antitopDecay = ERROR;
	   const xAOD::TruthParticle* topLepton = nullptr;
	   const xAOD::TruthParticle* antitopLepton = nullptr;
	   bool topfound = false;
	   bool antitopfound = false;

	   for (const auto& particle: *truthParticles) {

	     if (particle->pdgId() == 6){ //take always the last top
	       m_truth_top_pt = particle->pt();
               if(particle->nChildren()==2){ //requirment of nChlildren=2 added to avoid errors due to missing connections between top quarks
                 std::tie(topDecay, topLepton) = getTopDecayMode(particle);
	         topfound = true;  
               }
	     }
	     else if (particle->pdgId() == -6){ //take always the last top
	       m_truth_antitop_pt = particle->pt(); 
	       if (particle->nChildren()==2) { //same as for top
			std::tie(antitopDecay, antitopLepton) = getTopDecayMode(particle);
			antitopfound = true;
	       }
             }

	     if(topfound && topDecay == ERROR){ // if top found but the top decay cannot be classified there is a issue with missing connections between W's
	       if (particle->pdgId() ==24 && particle->nChildren()==2 && particle->nParents()==0){ // last W of decay chain-> no children => start at next W with no parents (has to be verified)
		 //Warning("savenEvent()","take next W(pdgId=24)");
		 std::tie(topDecay, topLepton) = getWDecayMode(particle);
	       }
	     }
	      if(antitopfound && antitopDecay==ERROR){
	     
	       if (particle->pdgId() ==-24 && particle->nChildren()==2 && particle->nParents()==0){
		 //Warning("saveEvent()","take next W(pdgId=-24)");
		 std::tie(antitopDecay, antitopLepton) = getWDecayMode(particle);
	       }
	     }
	     
	     if (topfound && antitopfound && topDecay !=ERROR && antitopDecay != ERROR) {
	       if (topDecay==TAU){
		 for (const auto& particle2: *truthParticles) {
		   //suppress semi-leptonic b-decays with the PDGid and the mass cut.
		   if (particle2->pdgId() == -15 && particle2->nParents() > 0 && particle2->parent(0)->p4().M()/1000. > 20) {
		     if (particle2->nChildren() > 1){
		       if (particle2->child(1)->isElectron()) topDecay = TAULEPel;
		       else if (particle2->child(1)->isMuon()) topDecay = TAULEPmu;
		       else {
			 if (particle2->child(0)->nChildren()>1){ //tau-> tau+photon
			   if (particle2->child(0)->child(1)->isElectron()) topDecay = TAULEPel; 
			   else if (particle2->child(0)->child(1)->isMuon()) topDecay = TAULEPmu;
			 }
			 else{
			   topDecay = TAUHAD;
			 }
		       }
                       break;
		     }
		   }
		 }
	       }
	       if (antitopDecay==TAU){
		 for (const auto& particle2: *truthParticles) {
		   //suppress semi-leptonic b-decays with the PDGid and the mass cut.
		   if (particle2->pdgId() == 15 && particle2->nParents() > 0 && particle2->parent(0)->p4().M()/1000. > 20) { 
		     if (particle2->nChildren() > 1){
		       if (particle2->child(1)->isElectron()) antitopDecay = TAULEPel;
		       else if (particle2->child(1)->isMuon()) antitopDecay = TAULEPmu;
		       else{
			 if (particle2->child(0)->nChildren()>1){
			   if (particle2->child(0)->child(1)->isElectron()) antitopDecay = TAULEPel; 
			   else if (particle2->child(0)->child(1)->isMuon()) antitopDecay = TAULEPmu; 
			 }
			 else{
			   antitopDecay = TAUHAD;
			 }
		       }
                       break;
		     }
		   }
		 }
	       }
	       break;
	     }
	   }

	   if (topDecay==TAU) topDecay = TAUHAD;
	   if (antitopDecay==TAU) antitopDecay = TAUHAD;
	
	   reducedDecayMode reducedtopDecay = rERROR;
	   reducedDecayMode reducedantitopDecay = rERROR;
	   if (topDecay==HAD) reducedtopDecay = rHAD;
	   else if (topDecay==LEPel || topDecay==LEPmu) reducedtopDecay = rLEP;
	   else if (topDecay==TAULEPel || topDecay==TAULEPmu) reducedtopDecay = rTAULEP;
	   else if (topDecay==TAUHAD) reducedtopDecay = rTAUHAD;
	
	   if (antitopDecay==HAD) reducedantitopDecay = rHAD;
	   else if (antitopDecay==LEPel || antitopDecay==LEPmu) reducedantitopDecay = rLEP;
	   else if (antitopDecay==TAULEPel || antitopDecay==TAULEPmu) reducedantitopDecay= rTAULEP;
	   else if (antitopDecay==TAUHAD) reducedantitopDecay= rTAUHAD;

	   Category ttbarCat = UNKNOWN;
	   if (!topfound || !antitopfound) {
	     Error("execute()", "failed to find two top quarks");
	   }
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rLEP, rLEP))
	     ttbarCat = LEP_LEP;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rLEP, rHAD))
	     ttbarCat = LEP_HAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rLEP, rTAUHAD))
	     ttbarCat = LEP_TAUHAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rLEP, rTAULEP))
	     ttbarCat = LEP_TAULEP;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rTAULEP, rHAD))
	     ttbarCat = TAULEP_HAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rTAULEP, rTAUHAD))
	     ttbarCat = TAULEP_TAUHAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rTAULEP, rTAULEP))
	     ttbarCat = TAULEP_TAULEP;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rHAD, rHAD))
	     ttbarCat = HAD_HAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rHAD, rTAUHAD))
	     ttbarCat = HAD_TAUHAD;
	   else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, rTAUHAD, rTAUHAD))
	     ttbarCat = TAUHAD_TAUHAD;
	   else {
	     Warning("execute()", "failed to find combined decay %d %d", topDecay, antitopDecay);
	   }

	   m_ttbar_cat = static_cast<int>(ttbarCat);
	 } // end ttbar classification
	 


	 
    } // end savetruth
    
    
    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  } // end save event
 







// ---------- define functions for usage in events saver------------------//

  // Explain function !!!!
 


  //Explain function!!!!
  int WupExotEventSaver::decayToChargedLepton(const xAOD::TruthParticle* tparticle)
  {
    if (tparticle == 0){
      std::cout << "Mother particle does not exist. That is quite bad." << std::endl;
      return false;
    }
    int idFirstChild = 0;
    int idSecondChild = 0;

     if(tparticle->nChildren()!=2){
      Error("decayToChargedLepton()", "do not found 2 children for LQ");
      return -999;
     }
     else if (tparticle->nChildren()==2){
    
       // it seems that in LQu sample the b-quark from the LQ decay is sometimes missing
        if(tparticle->child(0)==0){
	  //Warning("decayToChargedLepton()", "first child does not exist");
	 
        }
        else idFirstChild = tparticle->child(0)->absPdgId();
      
        if(tparticle->child(1)==0){
	  //Warning("decayToChargedLepton()", "second child does not exist");   
        }
        else idSecondChild = tparticle->child(1)->absPdgId();
    }
    std::cout << "idFirstChild: " << idFirstChild << " idSecondChild: " << idSecondChild << std::endl;
    if (idFirstChild == 11 || idFirstChild == 13 || idFirstChild == 15){
        return 1;
    }
    else if (idSecondChild == 11 || idSecondChild == 13 || idSecondChild == 15){ 
        return 1;
    }
    else if (idFirstChild == 0 && idSecondChild == 0){
       Warning("decayToChargedLepton()", "do not found any child; no classification possible!!!");
    }
    return 0;
  }

  int WupExotEventSaver::decayToChargedLeptonVLQ(const xAOD::TruthParticle* tparticle)
  {
    if (tparticle == 0){
      std::cout << "Mother particle does not exist. That is quite bad." << std::endl;
      return false;
    }
    int idFirstChild = 0;
    int idSecondChild = 0;

     if(tparticle->nChildren()!=2){
      Error("decayToChargedLeptonVLQ()", "did not find 2 children for LQ");
      return -999;
     }
     else if (tparticle->nChildren()==2){
    
       // it seems that in LQu sample the b-quark from the LQ decay is sometimes missing
        if(tparticle->child(0)==0){
	  //Warning("decayToChargedLepton()", "first child does not exist");
	 
        }
        else idFirstChild = tparticle->child(0)->absPdgId();
      
        if(tparticle->child(1)==0){
	  //Warning("decayToChargedLepton()", "second child does not exist");   
        }
        else idSecondChild = tparticle->child(1)->absPdgId();
    }
    std::cout << "decayToChargedLeptonVLQ(): " << tparticle->pdgId() << " idFirstChild: " << idFirstChild << " idSecondChild: " << idSecondChild << std::endl;
    if (idFirstChild == 5 || idSecondChild == 5){
        return 1;
    }
    if (idFirstChild == 6 || idSecondChild == 6){ 
        return 0;
    }
    if (idFirstChild == 0 && idSecondChild == 0){
       Warning("decayToChargedLeptonVLQ()", "do not found any child; no classification possible!!!");
    }
    else Warning("decayToChargedLeptonVLQ()", "Should never get here.");
    return 0;
  }

 

std::pair< WupExotEventSaver::DecayMode, const xAOD::TruthParticle*> WupExotEventSaver::getTopDecayMode(const xAOD::TruthParticle *top)
{
	for (size_t i = 0; i < top->nChildren(); i++) {
		auto particle = top->child(i);

		if (particle->isTop()) {
			return getTopDecayMode(particle);
		}

		if (particle->isW()) {
		  if (particle->nChildren()==0){
		    //Warning("getTopDecayMode()", "no W %d child found", particle->pdgId());
		     
				return std::make_pair(ERROR, nullptr);
		  }
			while (particle->child(0)->isW()){
				particle = particle->child(0);
				if (particle->nChildren()==0){
				   Warning("getTopDecayMode()", "no W child found 2");
				  return std::make_pair(ERROR, nullptr);}
			}

			for (size_t j = 0; j < particle->nChildren(); j++) {
				auto wBosonChild = particle->child(j);
				if (wBosonChild->isMuon()) {
					return std::make_pair(LEPmu, wBosonChild);
				}
				if (wBosonChild->isElectron()) {
					return std::make_pair(LEPel, wBosonChild);
				}
				else if (wBosonChild->isTau()) {
					return std::make_pair(TAU, wBosonChild);
				}
			}
			return std::make_pair(HAD, nullptr);
		}
	}
	Warning("getTopDecayMode()", "no W detected, top children %d", int(top->nChildren()));
	return std::make_pair(ERROR, nullptr);
}

std::pair< WupExotEventSaver::DecayMode, const xAOD::TruthParticle*> WupExotEventSaver::getWDecayMode(const xAOD::TruthParticle *W)
{
	for (size_t i = 0; i < W->nChildren(); i++) {
	
		
	  auto wBosonChild = W->child(i);
	  if (wBosonChild->isMuon()) {
	    return std::make_pair(LEPmu, wBosonChild);
	  }
	  if (wBosonChild->isElectron()) {
	    return std::make_pair(LEPel, wBosonChild);
	  }
	  else if (wBosonChild->isTau()) {
	    return std::make_pair(TAU, wBosonChild);
	  }
	  
	  return std::make_pair(HAD, nullptr);
			
	}
	return std::make_pair(ERROR, nullptr);
}

bool WupExotEventSaver::isCombinedDecay(const reducedDecayMode topDecay,const reducedDecayMode antitopDecay, const reducedDecayMode d1, const reducedDecayMode d2) {
	return ((topDecay == d1 && antitopDecay == d2) || (topDecay == d2 && antitopDecay == d1));
}


bool WupExotEventSaver::isNominalTree(std::string name){
    bool isNominal = (m_config->systematicName(m_config->nominalHashValue()) == name);
    isNominal |= ((m_config->systematicName(m_config->nominalHashValue()) + "_Loose" ) == name);

    return isNominal;
}
 
} // namespace top





