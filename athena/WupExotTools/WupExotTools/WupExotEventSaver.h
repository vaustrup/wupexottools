#ifndef WUPEXOTEventSaver_H
#define WUPEXOTEventSaver_H

#include "TopAnalysis/EventSaverFlatNtuple.h"

#include "xAODTruth/TruthParticleContainer.h"

#include "METInterface/IMETSignificance.h"
#include "JetResolution/IJERTool.h"

#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"

//Tool handles
#include "AsgTools/AnaToolHandle.h"






/**
 * This class is the WupExotEventSaver designed for a LQ and VLQ analysis
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
  class WupExotEventSaver : public top::EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      WupExotEventSaver();
      ///-- Destructor does nothing --///
      virtual ~WupExotEventSaver(){}
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize(){return StatusCode::SUCCESS;}      
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void saveEvent(const top::Event& event) override;
      
    private:
      ///-- Some additional variables for handling in the event saver--//
      std::shared_ptr<top::TopConfig> m_config;
      bool m_savetruth;

       //event info
      unsigned int m_lumiBlock;
      unsigned int m_npvtx;
      float m_vtxz;

      ///-- DSIDs of samples TT and BB
      std::vector<int> m_vlq_dsids {302469,302472,302476,302478,302480,302481,302482,308294,308295,308296,308297,308299,302483,302485,302487,302490,302494,302496,302498,302499,302500,308303,308304,308305,308306,308308,302501,302503}; //  VLQ DSIDS
      //std::vector<int> m_vlq_dsids_bb {302487,302490,302494,302496,302498,302499,302500,308303,308304,308305,308306,308308,302501,302503}; //  VLQ BB DSIDS
      std::vector<int> m_lq_dsids {309867,309886}; // first and last number of LQ DSIDS
      std::vector<int> m_lq_dsids_ext {312117,312200}; // first and last number of LQ DSIDS ext
      std::vector<int> m_vectorlq_dsids {502659,502722};
      std::vector<int> m_vectorlq_dsids_ext {509873, 509936};
      std::vector<int> m_truthmet_dsids {410470,407345,407346,407347,410557,410558,407357,407358,407359,410646,410647,411193,411194,411195,411196,411197,411198,410654,410655,411199,411200,411201,411202,411203,411204,600293,600294,600295,600296,600297,600298,600299,600300,600727,600728,600729,600730,600731,600732,600733,600734};
       // tt Powheg+ph8 , tt Powheg+Herwig, singletop pw+py8 DR, single top pw+py8 DS
      std::vector<int> m_ttbar_dsids {410470,407345,407346,407347};
      std::vector<int> m_ttbar_dsids_alt {410557,410558,410464,410465,410480,410482};
      
      ///-- Some additional custom variables for the output --///
      int m_chargedLeptonDecayLQ;
      int m_chargedLeptonDecayAntiLQ;

      double m_lq_truth_pt;
      double m_antilq_truth_pt;

      int m_nTruthJets;

      int m_vlq_evtype;
      int m_numZnunu;

      double m_truthmet;
      float m_GenFiltHTinclNu;
      float m_GenFiltHTnew;
      float m_GenFiltHT;

      int m_ttbar_cat;
      double m_truth_top_pt;
      double m_truth_antitop_pt;

      double m_met_sig;
      asg::AnaToolHandle<IMETSignificance> metSignif;

      double m_ht_sig;
      asg::AnaToolHandle<IJERTool> jerTool;
      double scale=100;
  
      bool m_passHighPt; 
      float m_SF_ID_HighPt;
      float m_SF_ID_STAT_UP_HighPt;
      float m_SF_ID_STAT_DOWN_HighPt;
      float m_SF_ID_SYST_UP_HighPt;
      float m_SF_ID_SYST_DOWN_HighPt;
      float m_SF_ID_STAT_LOWPT_UP_HighPt;
      float m_SF_ID_STAT_LOWPT_DOWN_HighPt;
      float m_SF_ID_SYST_LOWPT_UP_HighPt;
      float m_SF_ID_SYST_LOWPT_DOWN_HighPt;
      asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonEfficiencyScaleFactorToolHighPt;


      bool m_bad_muon_highpt;
      bool m_bad_muon_normal;
      asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolHighPt; 
      asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolNormal;
      
      enum Category {
	    LEP_LEP,
	    LEP_HAD,
	    LEP_TAUHAD,
	    LEP_TAULEP,
	    TAULEP_HAD,
	    TAULEP_TAUHAD,
	    TAULEP_TAULEP,
	    HAD_HAD,
	    HAD_TAUHAD,
	    TAUHAD_TAUHAD,
	    UNKNOWN,
       };

       enum DecayMode {HAD, LEPel, LEPmu, TAU, TAUHAD, TAULEPel, TAULEPmu, ERROR};
       enum reducedDecayMode {rHAD, rLEP, rTAU, rTAUHAD, rTAULEP, rERROR};



      // Define funtions used in the EventSaver
      bool isNominalTree(std::string name);

      int decayToChargedLepton(const xAOD::TruthParticle* tparticle);
      int decayToChargedLeptonVLQ(const xAOD::TruthParticle* tparticle);

      std::pair<DecayMode, const xAOD::TruthParticle*> getTopDecayMode(const xAOD::TruthParticle *top);
      std::pair<DecayMode, const xAOD::TruthParticle*> getWDecayMode(const xAOD::TruthParticle *W);
      bool isCombinedDecay(const reducedDecayMode topDecay,const reducedDecayMode antitopDecay, const reducedDecayMode d1, const reducedDecayMode d2);
      
      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDef(top::WupExotEventSaver, 0);
  };
}

#endif
