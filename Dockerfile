FROM atlas/analysisbase:21.2.151
ADD . /analysis
WORKDIR /analysis/build
RUN sudo chown -R atlas /analysis && \
    source ~/release_setup.sh && \
    cmake ../athena/Projects/WorkDir && \
    cmake --build ./
